﻿
namespace Lab_05
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCheck = new System.Windows.Forms.Button();
            this.buttonStartMultiThreading = new System.Windows.Forms.Button();
            this.buttonLoadDll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCheck
            // 
            this.buttonCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonCheck.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCheck.Location = new System.Drawing.Point(31, 83);
            this.buttonCheck.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonCheck.Name = "buttonCheck";
            this.buttonCheck.Size = new System.Drawing.Size(395, 47);
            this.buttonCheck.TabIndex = 8;
            this.buttonCheck.Text = "Перевірка бібліотеки Math ";
            this.buttonCheck.UseVisualStyleBackColor = false;
            this.buttonCheck.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // buttonStartMultiThreading
            // 
            this.buttonStartMultiThreading.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonStartMultiThreading.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartMultiThreading.Location = new System.Drawing.Point(31, 139);
            this.buttonStartMultiThreading.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonStartMultiThreading.Name = "buttonStartMultiThreading";
            this.buttonStartMultiThreading.Size = new System.Drawing.Size(395, 47);
            this.buttonStartMultiThreading.TabIndex = 7;
            this.buttonStartMultiThreading.Text = "Multithreading sort";
            this.buttonStartMultiThreading.UseVisualStyleBackColor = false;
            this.buttonStartMultiThreading.Click += new System.EventHandler(this.buttonStartMultiThreading_Click);
            // 
            // buttonLoadDll
            // 
            this.buttonLoadDll.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonLoadDll.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadDll.Location = new System.Drawing.Point(28, 33);
            this.buttonLoadDll.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonLoadDll.Name = "buttonLoadDll";
            this.buttonLoadDll.Size = new System.Drawing.Size(395, 42);
            this.buttonLoadDll.TabIndex = 6;
            this.buttonLoadDll.Text = "Завантажити dll";
            this.buttonLoadDll.UseVisualStyleBackColor = false;
            this.buttonLoadDll.Click += new System.EventHandler(this.buttonLoadDll_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 217);
            this.Controls.Add(this.buttonCheck);
            this.Controls.Add(this.buttonStartMultiThreading);
            this.Controls.Add(this.buttonLoadDll);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCheck;
        private System.Windows.Forms.Button buttonStartMultiThreading;
        private System.Windows.Forms.Button buttonLoadDll;
    }
}

