﻿using System;
using System.IO;
using System.Diagnostics;
using System.Management;
using System.Runtime.InteropServices;
using Microsoft.TeamFoundation.Common.Internal;
using Topshelf.Runtime.Windows;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            int flag = 0;
            for (; ; )
            {
                Console.WriteLine("Выберите, что необходимо сделать. \n 0. Закрыть приложение. \n 1. Вывести список всех логических дисков. \n 2. Получить тип каждого диска. \n 3. Получить информацию про диск и файловую систему, которая используется. \n 4. Вывести заполнение и свободное место на каждом диске. \n 5. Получить информацию про системную память. \n 6. Вывести название компьютера \n 7. Вывести имя текущего пользователя. \n 8. Вывести про поточный каталог. \n 9. Отслеживания в выбранном каталоге изменений.");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 0:
                        flag = 1;
                        break;
                    case 1:
                        Console.WriteLine(string.Join(", ", Environment.GetLogicalDrives()));
                        break;
                    case 2:
                        var drives = DriveInfo.GetDrives();
                        foreach (var drive in drives)
                        {
                            Console.WriteLine($"Диск {drive.Name}, тип диска: {drive.DriveType}");
                            if (Convert.ToString(drive.DriveType) == "Fixed")
                            {
                                Console.WriteLine("Привод является фиксированным диском.");
                            }
                            if (Convert.ToString(drive.DriveType) == "CDRom")
                            {
                                Console.WriteLine("Привод представляет собой оптический дисковод, такой как CD или DVD-ROM.");
                            }
                            if (Convert.ToString(drive.DriveType) == "Network")
                            {
                                Console.WriteLine("Диск является сетевым.");
                            }
                            if (Convert.ToString(drive.DriveType) == "NoRootDirectory")
                            {
                                Console.WriteLine("Диск не имеет корневого каталога.");
                            }
                            if (Convert.ToString(drive.DriveType) == "Ram")
                            {
                                Console.WriteLine("Привод представляет собой RAM-диск.");
                            }
                            if (Convert.ToString(drive.DriveType) == "Removable")
                            {
                                Console.WriteLine("Диск представляет собой съемное запоминающее устройство, например флэш-накопитель USB.");
                            }
                            if (Convert.ToString(drive.DriveType) == "Unknown")
                            {
                                Console.WriteLine("Тип привода неизвестен.");
                            }
                        }
                        break;
                    case 3:
                        drives = DriveInfo.GetDrives();
                        foreach (var drive in drives)
                        {
                            Console.WriteLine($"Диск {drive.Name}, файловая система: {drive.DriveFormat}");
                        }
                        break;
                    case 4:
                        drives = DriveInfo.GetDrives();
                        foreach (var drive in drives)
                        {
                            Console.WriteLine($"Диск {drive.Name}. Общий обьем диска: {drive.TotalSize}. Свободно на диске: {drive.TotalFreeSpace}. Занято на диске: {drive.TotalSize - drive.TotalFreeSpace}");
                        }
                        break;
                    case 5:
                        ObjectQuery wql = new ObjectQuery("SELECT * FROM Win32_OperatingSystem");
                        ManagementObjectSearcher searcher = new ManagementObjectSearcher(wql);
                        ManagementObjectCollection results = searcher.Get();

                        foreach (ManagementObject result in results)
                        {
                            Console.WriteLine("Total Visible Memory: {0} KB", result["TotalVisibleMemorySize"]);
                            Console.WriteLine("Free Physical Memory: {0} KB", result["FreePhysicalMemory"]);
                            Console.WriteLine("Total Virtual Memory: {0} KB", result["TotalVirtualMemorySize"]);
                            Console.WriteLine("Free Virtual Memory: {0} KB", result["FreeVirtualMemory"]);
                        }
                        break;
                    case 6:
                        Console.WriteLine("Имя компьютера: {0}", Environment.MachineName);
                        break;
                    case 7:
                        Console.WriteLine("Имя пользователя: {0}", Environment.UserName);
                        break;
                    case 8:
                        string path = Directory.GetCurrentDirectory();
                        Console.WriteLine("Текущий рабочий каталог: {0}", path);
                        string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        Console.WriteLine("Временный каталог: {0}", tempDirectory);
                        Console.WriteLine("Системный каталог: {0}", Environment.GetFolderPath(Environment.SpecialFolder.System));
                        break;
                    case 9:
                        Watching();
                        break;
                }
                if (flag == 1)
                {
                    break;
                }
            }
        }
        public static void Watching()
        {
            Console.WriteLine("Введите путь каталога для отслеживания");
            string path = Console.ReadLine();
            using var watcher = new FileSystemWatcher(path);
            watcher.NotifyFilter = NotifyFilters.Attributes
                                 | NotifyFilters.CreationTime
                                 | NotifyFilters.DirectoryName
                                 | NotifyFilters.FileName
                                 | NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.Security
                                 | NotifyFilters.Size;

            watcher.Changed += OnChanged;
            watcher.Created += OnCreated;
            watcher.Deleted += OnDeleted;
            watcher.Renamed += OnRenamed;
            watcher.Error += OnError;

            watcher.Filter = "";
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;

            Console.WriteLine("Press enter to exit.");
            Console.ReadLine();
        }
        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
            {
                return;
            }
            Console.WriteLine($"Changed: {e.FullPath}");
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(e.FullPath, w);
            }
        }

        private static void OnCreated(object sender, FileSystemEventArgs e)
        {
            string value = $"Created: {e.FullPath}";
            Console.WriteLine(value);
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log($"Создан файл: {value}", w);
            }
        }

        private static void OnDeleted(object sender, FileSystemEventArgs e) 
        { 
            Console.WriteLine($"Deleted: {e.FullPath}");
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log($"Удаление файла {e.FullPath}", w);
            }
        }
        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine($"Renamed:");
            Console.WriteLine($"    Old: {e.OldFullPath}");
            Console.WriteLine($"    New: {e.FullPath}");
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log($"Изменение имени файла {e.OldFullPath} на {e.FullPath}", w);
            }
        }

        private static void OnError(object sender, ErrorEventArgs e)
        {
            PrintException(e.GetException());
        }

        private static void PrintException(Exception? ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine();
                PrintException(ex.InnerException);
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log($"Ошибка {ex.InnerException}", w);
                }
            }
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }
        public static void DumpLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }
    }
}
