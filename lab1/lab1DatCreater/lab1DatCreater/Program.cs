﻿using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace lab1DatCreater
{
    class Program
    {
        static Mutex mutObj;
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[] arr = new int[20];
            bool check = true;
            var mmf = MemoryMappedFile.CreateFromFile(@"d:\testignLab1\data.data", FileMode.Create, "fileHandle", 1024 * 1024);
            var accessor = mmf.CreateViewAccessor();
            while (true)
            {

                ConsoleKeyInfo key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Spacebar)
                {
                    if (check)
                    {
                        mutObj = new Mutex(false, "Mutex");
                        check = false;
                    }
                    try
                    {
                        mutObj.WaitOne();
                        Console.Clear();
                        for (int i = 0; i < arr.Length; i++)
                        {
                            arr[i] = rnd.Next(10, 100);
                            Console.WriteLine($"{arr[i]}");
                        }

                        accessor.WriteArray(0, arr, 0, arr.Length);
                        Console.WriteLine("Массив создан");
                    }
                    finally
                    {
                        mutObj.ReleaseMutex();
                    }

                }
            }
        }
    }
}
