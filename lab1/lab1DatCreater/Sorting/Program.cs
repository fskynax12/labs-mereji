﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace Sorting
{
    class Program
    {
        static Mutex mutObj;

        static unsafe void Main(string[] args)
        {
            bool res = false;
            while (res == false)
            {
                res = Mutex.TryOpenExisting("Mutex", out mutObj);
            }
            bool fl;
            int c;
            var mmf = MemoryMappedFile.OpenExisting("fileHandle");
            var accessor = mmf.CreateViewAccessor();
            byte* ptr = null;
            accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref ptr);
            while (true)
            {

                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Spacebar)
                {

                    do
                    {
                        fl = false;
                        for (int i = 1; i < 20; i++)
                        {
                            try
                            {
                                mutObj.WaitOne();
                                if (*(int*)(ptr + (i - 1) * 4) > *(int*)(ptr + i * 4))
                                {
                                    c = *(int*)(ptr + i * 4);
                                    *(int*)(ptr + i * 4) = *(int*)(ptr + (i - 1) * 4);
                                    *(int*)(ptr + (i - 1) * 4) = c;
                                    fl = true;
                                    Thread.Sleep(1500);
                                }
                            }
                            finally
                            {
                                mutObj.ReleaseMutex();
                            }
                        }
                        Console.WriteLine("Конец сортировки");
                    } while (fl);


                }
            }
        }
    }
}
