﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShowData
{
    public unsafe partial class Form1 : Form
    {
        int[] arr = new int[20];
        static Mutex mutObj;
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private unsafe void Form1_Load(object sender, EventArgs e)
        {

        }
        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            bool res = false;
            while (res == false)
            {
                res = Mutex.TryOpenExisting("Mutex", out mutObj);
            }
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                mutObj.WaitOne();
                listBox1.Items.Clear();
                var mmf = MemoryMappedFile.OpenExisting("fileHandle");
                var accessor = mmf.CreateViewAccessor();
                byte* ptr = null;
                accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref ptr); 
                string k;
                for (int i = 0; i < 20; i++)
                {
                    k = "";
                    for (int j = 0; j < (*(int*)(ptr + i * 4)); j++)
                        k += "*";
                    listBox1.Items.Add(k);
                }
            }
            finally
            {
                mutObj.ReleaseMutex();
            }
        }
    }
}
